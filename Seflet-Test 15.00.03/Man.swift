//
//  Man.swift
//  HelooUltraPro
//
//  Created by Константин Черкашин on 20.08.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import Foundation

enum ManFields : String { // keys for server's responses
    case fname = "firstName"
    case lname = "lastName"
    
}

class Man: NSObject { // model of user
    private(set) var fname: String = ManFields.fname.rawValue
    
    init(json: NSDictionary) {
        
        if let someConstant = json[ManFields.fname.rawValue] as? String {
            self.fname = someConstant
        }
        
    }
    
}