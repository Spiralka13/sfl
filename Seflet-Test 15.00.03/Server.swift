//
//  Server.swift
//  HelooUltraPro
//
//  Created by Константин Черкашин on 01.06.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import Foundation
import Alamofire

enum URL_PATHS: String {
    case base_url = "http://losanatoly.16mb.com/"
    case login = "login"
}

class Server { //net requests
    static let makeRequest = Server()
    
    func login(username: String, password: String, completionHandler: (success: Bool) -> ()) {
        
        let url = URL_PATHS.base_url.rawValue + URL_PATHS.login.rawValue
        print("login url = \(url)")
        
        //http request here (alamofire for example)
        
        completionHandler(success: false)
    }
    
    
    
    
}